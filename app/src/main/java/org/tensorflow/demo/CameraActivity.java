/*
 * Copyright 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tensorflow.demo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;
import org.bytedeco.javacpp.tensorflow.*;

import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.TimerTask;

import static org.bytedeco.javacpp.tensorflow.*;

public class CameraActivity extends Activity {
    private static final int PERMISSIONS_REQUEST = 1;

    private static final String PERMISSION_CAMERA = Manifest.permission.CAMERA;
    private static final String PERMISSION_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private final String TAG = "ActionRecognizer";
    private final double[] SENSOR_0_MEAN = new double[]{0.22612935, 0.2253594, 0.22416563, 0.22598018, 0.22587056,
            0.22821029, 0.22833209, 0.2291304, 0.22824351, 0.22712676,
            0.22767429, 0.22535878, 0.22559683, 0.22493818, 0.22429581,
            0.224684, 0.22433499, 0.22401509, 0.22313856, 0.22047754,
            0.21845338, 0.21894053, 0.21916062, 0.21679269, 0.21638855,
            0.21816111, 0.21637923, 0.21707679, 0.2170101, 0.21851972,
            0.2140464, 0.21306191, 0.20994116, 0.2091203, 0.20993173,
            0.21128778, 0.21180983, 0.20994784, 0.20882393, 0.20745492,
            0.20750636, 0.20708193, 0.20562556, 0.20477757, 0.20477198,
            0.20228943, 0.20405735, 0.20657574, 0.20483728, 0.20425764,
            0.2026376, 0.20253998, 0.20209233, 0.19901891, 0.19953272,
            0.19838771, 0.19855654, 0.19684643, 0.19655915, 0.19479871,
            0.1938967, 0.1936259, 0.19422454, 0.19544177, 0.19593284,
            0.19455598, 0.19307064, 0.19272964, 0.18981874, 0.19078238,
            0.19201791, 0.18926024, 0.19036363, 0.19057158, 0.19077784};
    private final double[] SENSOR_0_STD = new double[]{1.29931486, 1.30149662, 1.3077656, 1.31095493, 1.31523407,
            1.31809497, 1.32357776, 1.32801163, 1.32894504, 1.33501875,
            1.33484781, 1.33727396, 1.34087312, 1.33973908, 1.33804774,
            1.3409102, 1.34058917, 1.34132802, 1.33801043, 1.33736527,
            1.33931839, 1.34474456, 1.34315848, 1.34813797, 1.34526992,
            1.34142339, 1.33999693, 1.34053159, 1.34404933, 1.34615088,
            1.34590626, 1.347857, 1.35132265, 1.35246062, 1.3553803,
            1.35447752, 1.34859622, 1.35247719, 1.35169649, 1.35049343,
            1.35232401, 1.35342956, 1.35948229, 1.35730541, 1.35430253,
            1.35347462, 1.34669852, 1.34395015, 1.33996093, 1.33461905,
            1.33522713, 1.33558154, 1.33407032, 1.33686829, 1.33531046,
            1.33485079, 1.33783853, 1.33985484, 1.33825421, 1.33663392,
            1.33917403, 1.32990742, 1.32868397, 1.3218466, 1.31621563,
            1.314327, 1.31285727, 1.31123781, 1.31292713, 1.31019986,
            1.30733716, 1.30582428, 1.30423331, 1.30304205, 1.30158007};
    private final double[] SENSOR_1_MEAN = new double[]{9.10185814, 9.10324383, 9.09983063, 9.1031065, 9.10574055,
            9.10685158, 9.1088419, 9.10784817, 9.10107613, 9.10252666,
            9.09597015, 9.090312, 9.08430481, 9.07646751, 9.07296371,
            9.07887173, 9.08556461, 9.08747864, 9.08158684, 9.07956982,
            9.08446789, 9.09156036, 9.09601974, 9.09455395, 9.08380699,
            9.08046436, 9.09871578, 9.10824966, 9.10207367, 9.09971619,
            9.09479523, 9.09076405, 9.09544659, 9.10001469, 9.09255314,
            9.09279442, 9.08789349, 9.08734512, 9.0956955, 9.09442234,
            9.09111881, 9.09766483, 9.09986305, 9.09264469, 9.09159279,
            9.09703255, 9.09538078, 9.09962559, 9.09935474, 9.09818649,
            9.10336971, 9.1066494, 9.11262608, 9.1052742, 9.10145664,
            9.10725975, 9.11408424, 9.11372471, 9.11169243, 9.11034584,
            9.11071587, 9.1150341, 9.11906242, 9.11650372, 9.10715771,
            9.10871696, 9.11677647, 9.1166935, 9.1114006, 9.11282539,
            9.11623573, 9.11598396, 9.115798, 9.12245178, 9.12095833};
    private final double[] SENSOR_1_STD = new double[]{3.51565862, 3.52651286, 3.54082799, 3.56223035, 3.57537246,
            3.58505154, 3.5993154, 3.60497093, 3.63048959, 3.63863277,
            3.61563182, 3.62521911, 3.63210487, 3.63319302, 3.63359928,
            3.63864756, 3.63951874, 3.6377039, 3.63724828, 3.63709497,
            3.63288498, 3.63435817, 3.631742, 3.62853765, 3.62230778,
            3.62794375, 3.63103461, 3.6262207, 3.6295805, 3.63018513,
            3.63254309, 3.63641119, 3.63842797, 3.6415875, 3.64053464,
            3.64065766, 3.64003754, 3.63689375, 3.63977313, 3.63125134,
            3.63570476, 3.62279987, 3.624017, 3.61770606, 3.61977315,
            3.61401176, 3.61197019, 3.60911369, 3.60070848, 3.59976435,
            3.59328341, 3.59043288, 3.5842762, 3.57996583, 3.57941175,
            3.57510138, 3.5738349, 3.57145572, 3.57068253, 3.55905962,
            3.55207872, 3.54631186, 3.546731, 3.54347777, 3.54304266,
            3.53825665, 3.53464365, 3.52886891, 3.53012657, 3.52052832,
            3.51983237, 3.51819992, 3.51084232, 3.5474329, 3.53909326};
    private final double[] SENSOR_2_MEAN = new double[]{-0.59835541, -0.59357214, -0.59075731, -0.59126407, -0.59209973,
            -0.59142405, -0.59242678, -0.59051645, -0.59370708, -0.59920812,
            -0.59968865, -0.60656148, -0.60950178, -0.61203456, -0.61411005,
            -0.62010086, -0.62247491, -0.62615782, -0.62569696, -0.62439787,
            -0.62203723, -0.62369388, -0.62046343, -0.62382126, -0.62414652,
            -0.61987185, -0.62133449, -0.62216699, -0.62098032, -0.62455362,
            -0.62922144, -0.63093919, -0.63334948, -0.63276112, -0.63067752,
            -0.63115108, -0.63133717, -0.6352911, -0.63976127, -0.64074695,
            -0.64324725, -0.64816082, -0.64615721, -0.64525735, -0.64705104,
            -0.6533919, -0.65156722, -0.64600682, -0.64334458, -0.64221686,
            -0.64260751, -0.63813692, -0.63084573, -0.63319552, -0.63192183,
            -0.63450748, -0.63384628, -0.63218492, -0.63405788, -0.63389802,
            -0.6368733, -0.63374233, -0.63587177, -0.63348341, -0.63089663,
            -0.63127911, -0.63595521, -0.63546157, -0.63451427, -0.63538557,
            -0.63862836, -0.64217925, -0.64386958, -0.64377177, -0.64198679};
    private final double[] SENSOR_2_STD = new double[]{3.27384019, 3.27903128, 3.28262234, 3.29606009, 3.30380845,
            3.30884099, 3.31969094, 3.31451273, 3.31147051, 3.31664109,
            3.31204247, 3.32290673, 3.31664038, 3.31712246, 3.31330252,
            3.31906772, 3.32061172, 3.32073593, 3.31007314, 3.31262922,
            3.30878687, 3.31895137, 3.31565356, 3.32939005, 3.32691264,
            3.32657599, 3.33486462, 3.33766413, 3.32839012, 3.32628965,
            3.32968521, 3.33273458, 3.3321867, 3.33447933, 3.33467078,
            3.33838677, 3.33890414, 3.34886432, 3.34866476, 3.35438943,
            3.35576677, 3.36055446, 3.36044312, 3.35289526, 3.34705639,
            3.35016561, 3.34503531, 3.32890773, 3.31832838, 3.31249809,
            3.3142488, 3.32055163, 3.32035995, 3.32568526, 3.32238889,
            3.32185555, 3.32804942, 3.32024932, 3.32387924, 3.32937026,
            3.33196735, 3.32433176, 3.32996058, 3.31747746, 3.30401707,
            3.29884839, 3.30842423, 3.3037219, 3.30072951, 3.29392672,
            3.29276133, 3.28924036, 3.29218435, 3.29040837, 3.28856349};
    private final double[] SENSOR_3_MEAN = new double[]{4.34000557e-03, 5.96337393e-03, 6.63864892e-03,
            6.60147937e-03, 8.11151043e-03, 9.36959777e-03,
            1.04429200e-02, 1.12338681e-02, 1.14684450e-02,
            1.07219676e-02, 1.18048349e-02, 1.04557378e-02,
            1.12080341e-02, 1.13107665e-02, 1.25629669e-02,
            1.14138629e-02, 1.00450646e-02, 6.81227399e-03,
            7.29929842e-03, 6.84967265e-03, 5.58621809e-03,
            7.14171678e-03, 6.82952162e-03, 5.85140241e-03,
            6.07119966e-03, 7.36310566e-03, 6.77908072e-03,
            8.32046568e-03, 9.22621228e-03, 9.34407115e-03,
            6.87237410e-03, 6.75176550e-03, 8.22282210e-03,
            9.48895607e-03, 9.34625138e-03, 6.73919963e-03,
            6.91608619e-03, 5.46459248e-03, 5.18182665e-03,
            5.56261279e-03, 3.54183628e-03, 2.95573683e-03,
            1.07072550e-03, 1.37317216e-03, 1.50241808e-03,
            8.64739050e-05, 2.01137323e-06, 1.54695101e-03,
            5.93069009e-04, -2.95342616e-04, -6.50644826e-04,
            3.56539851e-04, -2.89315445e-04, 2.24436939e-04,
            1.66636787e-03, 8.98487458e-04, 1.10077939e-03,
            2.97644315e-03, 2.82076048e-03, 2.74164369e-03,
            2.05425778e-03, 3.85415927e-03, 3.17493756e-03,
            2.70442618e-03, 6.86340732e-03, 7.34836189e-03,
            5.76707534e-03, 6.53524138e-03, 7.24162860e-03,
            6.67747995e-03, 5.72386011e-03, 6.16197474e-03,
            7.23802205e-03, 6.77871378e-03, 6.95562782e-03};
    private final double[] SENSOR_3_STD = new double[]{1.08705485, 1.08780801, 1.08964956, 1.09533441, 1.10172033,
            1.10586524, 1.10991132, 1.10789108, 1.11097252, 1.11652744,
            1.11795807, 1.12180102, 1.12148106, 1.12271607, 1.12480545,
            1.12930906, 1.12843668, 1.12900043, 1.12549949, 1.12573862,
            1.12373459, 1.12393677, 1.11981559, 1.11973131, 1.12065554,
            1.12100804, 1.12462687, 1.12391102, 1.12339127, 1.12240946,
            1.12298572, 1.12356234, 1.12711871, 1.12944365, 1.12883508,
            1.12999666, 1.1261853, 1.13028872, 1.12973213, 1.13098347,
            1.12969208, 1.12728179, 1.12911236, 1.13359368, 1.13535082,
            1.13786197, 1.13680422, 1.13240421, 1.13253117, 1.1308521,
            1.12791848, 1.12815428, 1.12451041, 1.12442672, 1.1217134,
            1.12301195, 1.12267447, 1.11933005, 1.1182375, 1.11867976,
            1.11753428, 1.11593997, 1.11893284, 1.11484003, 1.1002667,
            1.09920382, 1.09893346, 1.09597719, 1.09557593, 1.09653974,
            1.09562433, 1.09439576, 1.09073412, 1.09429061, 1.09060526};
    private final double[] SENSOR_4_MEAN = new double[]{-7.78398709e-04, 4.71105974e-04, 2.48852652e-03,
            3.32722184e-03, 2.83954805e-03, 2.63518235e-03,
            1.33782555e-03, 1.97859923e-03, 9.99066979e-04,
            7.75326771e-05, -2.45747506e-04, -5.52406636e-05,
            -1.63080316e-04, -1.49393454e-03, -1.48866768e-03,
            -1.68985710e-03, -1.52580254e-03, 4.90249484e-04,
            7.28979066e-04, 6.68893394e-04, 2.00415976e-04,
            1.52191543e-03, 1.38517981e-03, 2.62485072e-03,
            1.88708329e-03, 1.53840403e-03, 1.92025211e-03,
            6.20566250e-04, 6.02156215e-04, 1.67152099e-03,
            1.68206275e-03, 3.23502673e-03, 3.46720568e-03,
            3.70337744e-03, 4.55486169e-03, 3.58306547e-03,
            2.97122984e-03, 2.44315527e-03, 2.01959140e-03,
            -4.48449777e-04, -4.20814700e-04, -3.52419971e-04,
            -1.67415652e-03, -3.30250338e-03, -3.01295426e-03,
            -3.06687620e-03, -3.14001925e-03, -4.17275075e-03,
            -3.16610187e-03, -3.07251862e-03, -2.89234845e-03,
            -2.14861077e-03, -2.01620790e-03, 4.07019652e-05,
            4.32914734e-04, 1.41830067e-03, 6.63200335e-04,
            5.79436950e-04, 2.42785364e-03, 3.08500463e-03,
            4.60940832e-03, 4.53449925e-03, 3.39592155e-03,
            3.04477452e-03, 2.50906451e-03, 2.67363852e-03,
            2.39517167e-03, 3.69754457e-03, 4.28611878e-03,
            4.08230629e-03, 3.55600542e-03, 1.27510261e-03,
            6.35774282e-04, 1.50460459e-04, -9.81163001e-04};
    private final double[] SENSOR_4_STD = new double[]{0.82821858, 0.83403164, 0.83491099, 0.83791131, 0.84372455,
            0.84642047, 0.84941715, 0.85048431, 0.85577941, 0.8603738,
            0.86345118, 0.86711204, 0.86954999, 0.87297302, 0.87486571,
            0.87674826, 0.87584752, 0.87632751, 0.87475449, 0.87529677,
            0.87823719, 0.88326651, 0.88435304, 0.88545024, 0.8838923,
            0.88317633, 0.88769168, 0.88729131, 0.88798141, 0.88834804,
            0.88953853, 0.89110643, 0.89244479, 0.89127421, 0.89232528,
            0.88992113, 0.88524586, 0.88921618, 0.88931668, 0.89061618,
            0.89068788, 0.89147729, 0.88849378, 0.88947058, 0.88670856,
            0.88772374, 0.88587207, 0.8857345, 0.88310176, 0.88253754,
            0.87921125, 0.87711269, 0.87774026, 0.8805716, 0.87914145,
            0.87981158, 0.87920249, 0.8773458, 0.87488002, 0.87459594,
            0.87646401, 0.87277007, 0.87362003, 0.87001413, 0.86074668,
            0.86161244, 0.86363083, 0.86398596, 0.86077636, 0.86133063,
            0.86164606, 0.86235458, 0.8607797, 0.86424249, 0.86263663};
    private final double[] SENSOR_5_MEAN = new double[]{-0.00632781, -0.00591682, -0.00578881, -0.00594512, -0.00669913,
            -0.00753548, -0.007751, -0.00812877, -0.00800355, -0.00731758,
            -0.00729508, -0.00820487, -0.00880976, -0.00939544, -0.00968304,
            -0.00912142, -0.00857048, -0.00746713, -0.00828694, -0.00831434,
            -0.0083549, -0.00766582, -0.00683047, -0.00577527, -0.00564579,
            -0.00650608, -0.00610503, -0.00676531, -0.00681662, -0.00637779,
            -0.00583431, -0.00553031, -0.00547866, -0.00551218, -0.00555763,
            -0.00557711, -0.0054532, -0.00573073, -0.0053385, -0.00533169,
            -0.00515062, -0.00563204, -0.00536827, -0.00586013, -0.00532824,
            -0.00493704, -0.0052911, -0.00603124, -0.00544573, -0.00548267,
            -0.00495536, -0.00515555, -0.0037154, -0.00351005, -0.0039508,
            -0.00378978, -0.00369838, -0.0032949, -0.00342213, -0.00378654,
            -0.0040315, -0.00461907, -0.00482393, -0.00464148, -0.00484639,
            -0.00469122, -0.00512711, -0.00511593, -0.00540672, -0.00596771,
            -0.00558204, -0.00537802, -0.00538437, -0.00522285, -0.00460352};
    private final double[] SENSOR_5_STD = new double[]{0.44121018, 0.44223166, 0.44328797, 0.44464782, 0.44666204,
            0.4472605, 0.44925135, 0.45064294, 0.45171854, 0.45322773,
            0.44801906, 0.44991988, 0.45017579, 0.45338556, 0.4532668,
            0.45349404, 0.45327833, 0.45144874, 0.45124939, 0.4504343,
            0.45080337, 0.45100003, 0.45166865, 0.45057747, 0.45014107,
            0.45052812, 0.44935161, 0.45072228, 0.45052776, 0.45015752,
            0.44985279, 0.44956544, 0.44842407, 0.44654557, 0.44781142,
            0.44860291, 0.44830632, 0.44880155, 0.44817895, 0.44705406,
            0.44407037, 0.44544047, 0.44499162, 0.44420204, 0.44528636,
            0.44264522, 0.4410775, 0.44084722, 0.44103476, 0.44074088,
            0.4404791, 0.43783805, 0.43683916, 0.43625617, 0.43619427,
            0.43623856, 0.43532324, 0.43469718, 0.43507913, 0.43341008,
            0.43282121, 0.43224576, 0.43186593, 0.43078631, 0.42737246,
            0.42683908, 0.42606452, 0.42464596, 0.42387241, 0.42287904,
            0.42244142, 0.42199898, 0.42202714, 0.42164683, 0.42121729};
    private final double[] SENSOR_6_MEAN = new double[]{-0.68809241, -0.67757595, -0.66588962, -0.6623053, -0.66705602,
            -0.66568744, -0.66696906, -0.66422665, -0.66258883, -0.65661079,
            -0.65081161, -0.64244121, -0.63063568, -0.6187253, -0.60254806,
            -0.58673525, -0.57045144, -0.55795026, -0.54723775, -0.53464097,
            -0.51991177, -0.49998772, -0.47908637, -0.45757782, -0.43851745,
            -0.42311698, -0.40716597, -0.39328101, -0.38065499, -0.36720034,
            -0.35665157, -0.34976396, -0.34472364, -0.34001863, -0.33948636,
            -0.33768791, -0.33450618, -0.33230454, -0.32906541, -0.32417005,
            -0.31613588, -0.31265739, -0.30960247, -0.30352679, -0.29889587,
            -0.29456529, -0.2929081, -0.29409513, -0.29733169, -0.29795712,
            -0.29567987, -0.28979975, -0.28813997, -0.28724778, -0.2911312,
            -0.29993603, -0.31599286, -0.32906669, -0.34182063, -0.34925652,
            -0.355028, -0.36457556, -0.37451097, -0.38259548, -0.39163917,
            -0.40291348, -0.41148576, -0.42087191, -0.42767784, -0.43498656,
            -0.4430379, -0.45371819, -0.46509278, -0.47504193, -0.48535162};
    private final double[] SENSOR_6_STD = new double[]{25.56354332, 25.58724594, 25.60555077, 25.60012817,
            25.58573723, 25.5698967, 25.55634117, 25.54331589,
            25.53264999, 25.52110672, 25.5065937, 25.491642,
            25.47810936, 25.46755219, 25.45420456, 25.43884659,
            25.4234333, 25.40841293, 25.39544868, 25.37919235,
            25.36172485, 25.34342194, 25.32246017, 25.29932213,
            25.27763939, 25.25764847, 25.2360096, 25.22190094,
            25.20476151, 25.18423653, 25.16477203, 25.14432907,
            25.12538719, 25.1096611, 25.09315681, 25.07803154,
            25.06586838, 25.05542946, 25.04446411, 25.0327301,
            25.02104759, 25.00797081, 24.9949131, 24.98194504,
            24.96477509, 24.948946, 24.93567276, 24.92387962,
            24.91476631, 24.90839005, 24.89782906, 24.88664055,
            24.87493515, 24.86514091, 24.85578918, 24.84510422,
            24.83457756, 24.82076836, 24.80543518, 24.79251862,
            24.7805748, 24.76992798, 24.76075745, 24.75257683,
            24.74261475, 24.73069, 24.71931076, 24.70546913,
            24.68828773, 24.67372322, 24.66103745, 24.65014076,
            24.63741875, 24.62352371, 24.61184311};
    private final double[] SENSOR_7_MEAN = new double[]{-12.20306873, -12.21209335, -12.21914864, -12.23242283,
            -12.23976612, -12.24126244, -12.23949146, -12.2382803,
            -12.23823547, -12.2438097, -12.25286961, -12.2592535,
            -12.26287174, -12.26926422, -12.27843666, -12.28590202,
            -12.29673481, -12.3008852, -12.30743122, -12.31743336,
            -12.32454109, -12.33582687, -12.34221077, -12.3471241,
            -12.3483448, -12.35015202, -12.3501873, -12.35271263,
            -12.35687447, -12.35803223, -12.36058617, -12.3633604,
            -12.36417198, -12.36624908, -12.36829853, -12.37400723,
            -12.37861538, -12.3821516, -12.38993835, -12.4023838,
            -12.41048145, -12.4122076, -12.41316509, -12.411129,
            -12.40810585, -12.40799046, -12.40710354, -12.40446758,
            -12.39878654, -12.39256191, -12.38574505, -12.38249874,
            -12.37315083, -12.36885357, -12.36545086, -12.3572607,
            -12.35287666, -12.35781002, -12.36471176, -12.36937046,
            -12.37337208, -12.3756237, -12.36987305, -12.36505032,
            -12.36506176, -12.36828327, -12.3659296, -12.36147976,
            -12.36016655, -12.35687447, -12.35417557, -12.34796715,
            -12.33895016, -12.33063889, -12.31931114};
    private final double[] SENSOR_7_STD = new double[]{10.60421276, 10.60875607, 10.61746407, 10.6230917,
            10.63436985, 10.64620018, 10.65320301, 10.65617943,
            10.66089249, 10.66236496, 10.65857601, 10.65712357,
            10.65499306, 10.65595722, 10.65523243, 10.65370274,
            10.64935875, 10.64663601, 10.64506245, 10.64226627,
            10.63605499, 10.62948799, 10.62704754, 10.62440681,
            10.62058067, 10.6165638, 10.61490059, 10.61125183,
            10.60717106, 10.60641766, 10.60484505, 10.59959126,
            10.59112167, 10.58936405, 10.58684444, 10.58290291,
            10.57559204, 10.56523323, 10.55491734, 10.54624653,
            10.53730297, 10.53332996, 10.53242493, 10.52442074,
            10.51973152, 10.52006245, 10.51253986, 10.50532627,
            10.50127792, 10.4925909, 10.4841404, 10.4780817,
            10.47521782, 10.46808815, 10.46435356, 10.45983696,
            10.45269489, 10.44217014, 10.43725109, 10.43233109,
            10.42229462, 10.40957069, 10.40174675, 10.39579105,
            10.38785839, 10.38028145, 10.38320923, 10.38770771,
            10.38698673, 10.39157581, 10.39346886, 10.39184475,
            10.38875675, 10.39197063, 10.39617825};
    private final double[] SENSOR_8_MEAN = new double[]{-5.25408125, -5.27106762, -5.28889942, -5.31416464, -5.33780622,
            -5.3589201, -5.38311529, -5.4100337, -5.44297171, -5.47211266,
            -5.50169611, -5.52982235, -5.55718851, -5.58284569, -5.60948801,
            -5.63672495, -5.66293859, -5.68581724, -5.72097301, -5.7526536,
            -5.78078651, -5.80656624, -5.83566761, -5.86592007, -5.89579535,
            -5.91832638, -5.94288445, -5.96901035, -5.99796057, -6.02609825,
            -6.04905081, -6.07000971, -6.08682251, -6.09972668, -6.11108494,
            -6.11877155, -6.12821054, -6.1387682, -6.15458918, -6.16887331,
            -6.18041325, -6.18806076, -6.19345236, -6.2052598, -6.21848249,
            -6.22885847, -6.23931217, -6.25274467, -6.27071095, -6.29297829,
            -6.31187057, -6.32987213, -6.34587145, -6.36484861, -6.38393879,
            -6.40375757, -6.4228487, -6.44152594, -6.46340799, -6.48004007,
            -6.48221588, -6.48025179, -6.47018337, -6.46626425, -6.46266651,
            -6.46090937, -6.45902681, -6.45734119, -6.45481014, -6.45403242,
            -6.45126915, -6.4530201, -6.45499563, -6.45432329, -6.45550013};
    private final double[] SENSOR_8_STD = new double[]{35.9500351, 35.95353317, 35.95754242, 35.97900772,
            35.99466705, 36.00923538, 36.02619934, 36.03833008,
            36.05532837, 36.06713104, 36.08095169, 36.0951004,
            36.11128998, 36.12297821, 36.13452911, 36.14923096,
            36.16065979, 36.17094803, 36.18492889, 36.1983223,
            36.21049118, 36.22063446, 36.23649216, 36.25251007,
            36.27168274, 36.28801346, 36.30330276, 36.3203392,
            36.33967209, 36.36353683, 36.38673019, 36.40911484,
            36.42798996, 36.44451141, 36.46198273, 36.47455215,
            36.48664474, 36.49816132, 36.51098633, 36.52119446,
            36.5274086, 36.53409958, 36.53843307, 36.54857254,
            36.5569191, 36.56462479, 36.57398987, 36.58763885,
            36.60216904, 36.62216187, 36.6387825, 36.65645981,
            36.67475128, 36.68989944, 36.7064209, 36.72425461,
            36.74422836, 36.7616539, 36.78252792, 36.79916763,
            36.80156326, 36.78882599, 36.7737999, 36.7673378,
            36.76184464, 36.75348663, 36.74726868, 36.74494171,
            36.7450943, 36.74394608, 36.73971176, 36.73842239,
            36.74267197, 36.74335861, 36.74468994};

    private final String INPUT_STRING_LSTM = "placeholder_x:0";
    private final String OUTPUT_STRING_LSTM = "output_softmax:0";

    private Handler mInferHandler = new Handler();
    private SensorListener mListener = new SensorListener();
    private Session sessionSensor;
    private Tensor sensorTensor = new Tensor(DT_FLOAT, new TensorShape(1, 75, 9));
    private FloatBuffer sensorBuffer = sensorTensor.createBuffer();
    private final String LSTM_GRAPH_FILE = "LSTM_sensors_1483885066_thefinalone.pb";
    private final String[] labelsLSTM = {"downstairs", "eating", "jumping", "running", "sitting", "standing", "still", "upstairs", "walking"};

    private Session LoadGraph(String graphFileName) throws RuntimeException {
        GraphDef graphDef = new GraphDef();

        Status loadGraphStatus = ReadBinaryProto(Env.Default(), getGraphFilePath(getApplicationContext()) + "/" + graphFileName, graphDef);

        if (!loadGraphStatus.ok()) {
            Log.e("LoadGraph", loadGraphStatus.error_message().getString());
            throw new RuntimeException("Failed to load compute graph. Filename: " + graphFileName);
        }
        final Session session = new Session(new SessionOptions());
        Status sessionCreateStatus = session.Create(graphDef);
        if (!sessionCreateStatus.ok()) {
            Log.e("LoadGraph", sessionCreateStatus.error_message().getString());
            throw new RuntimeException("Failed to start session. Filename: " + graphFileName);
        }

        return session;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_camera);
        SensorManager mSenseManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor mAccel = mSenseManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Sensor mGyro = mSenseManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        Sensor mMag = mSenseManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sessionSensor = LoadGraph(LSTM_GRAPH_FILE);

        // Register sensor listeners with sampling frequency for gaming
        mSenseManager.registerListener(mListener, mAccel, SensorManager.SENSOR_DELAY_GAME);
        mSenseManager.registerListener(mListener, mGyro, SensorManager.SENSOR_DELAY_GAME);
        mSenseManager.registerListener(mListener, mMag, SensorManager.SENSOR_DELAY_GAME);

        if (hasPermission()) {
            if (null == savedInstanceState) {
                setFragment();
            }
        } else {
            requestPermission();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    setFragment();
                } else {
                    requestPermission();
                }
            }
        }
    }

    private boolean hasPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(PERMISSION_CAMERA) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(PERMISSION_STORAGE) == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (shouldShowRequestPermissionRationale(PERMISSION_CAMERA) || shouldShowRequestPermissionRationale(PERMISSION_STORAGE)) {
                Toast.makeText(CameraActivity.this, "Camera AND storage permission are required for this demo", Toast.LENGTH_LONG).show();
            }
            requestPermissions(new String[]{PERMISSION_CAMERA, PERMISSION_STORAGE}, PERMISSIONS_REQUEST);
        }
    }

    private void setFragment() {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, CameraConnectionFragment.newInstance())
                .commit();
    }

    private String getGraphFilePath(Context context) {
        String path;

        try {

            path = context.getExternalFilesDir(null).getAbsolutePath() + "/";
        } catch (NullPointerException e) {
            Log.d(context.getPackageName() + "-getGraphFilePath", "File can't be found");
            path = "";
        }
        return path;
    }

    private class SensorListener implements SensorEventListener {
        // Temporal window size
        private final String TAG = "SensorListener";
        private final int WINDOW_SIZE = 75;
        private final int SENSOR_NUM = 3; // 0 = accel, 1 = gyro, 2 = magnetic
        private final int AXIS_NUM = 3; //  0 = x, 1 = y, 2 = z
        private final int SENSOR_ACCEL = 0;
        private final int SENSOR_GYRO = 1;
        private final int SENSOR_MAG = 2;
        private final int AXIS_X = 0;
        private final int AXIS_Y = 1;
        private final int AXIS_Z = 2;
        private float[][][] mValArray = new float[SENSOR_NUM][AXIS_NUM][WINDOW_SIZE];
        private int currentTimeStep;
        private boolean[] timeStepComplete = new boolean[3];

        @Override
        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_ACCELEROMETER:
                    mValArray[SENSOR_ACCEL][AXIS_X][currentTimeStep] = event.values[0];
                    mValArray[SENSOR_ACCEL][AXIS_Y][currentTimeStep] = event.values[1];
                    mValArray[SENSOR_ACCEL][AXIS_Z][currentTimeStep] = event.values[2];
                    timeStepComplete[SENSOR_ACCEL] = true;
                    break;

                case Sensor.TYPE_GYROSCOPE:
                    mValArray[SENSOR_GYRO][AXIS_X][currentTimeStep] = event.values[0];
                    mValArray[SENSOR_GYRO][AXIS_Y][currentTimeStep] = event.values[1];
                    mValArray[SENSOR_GYRO][AXIS_Z][currentTimeStep] = event.values[2];
                    timeStepComplete[SENSOR_GYRO] = true;
                    break;

                case Sensor.TYPE_MAGNETIC_FIELD:
                    mValArray[SENSOR_MAG][AXIS_X][currentTimeStep] = event.values[0];
                    mValArray[SENSOR_MAG][AXIS_Y][currentTimeStep] = event.values[1];
                    mValArray[SENSOR_MAG][AXIS_Z][currentTimeStep] = event.values[2];
                    timeStepComplete[SENSOR_MAG] = true;
                    break;

            }
            // Use flags, if all are true, move to next, then reset flags
            if (allTrue(timeStepComplete)) {
                currentTimeStep++;
                timeStepComplete[SENSOR_ACCEL] = false;
                timeStepComplete[SENSOR_GYRO] = false;
                timeStepComplete[SENSOR_MAG] = false;
            }

            // Reset the current time step when current time step reaches window size
            if (currentTimeStep == WINDOW_SIZE && !ScoreFusion.getInstance().isSensorDataLocked()) {
                currentTimeStep = 0;
                timeStepComplete[SENSOR_ACCEL] = false;
                timeStepComplete[SENSOR_GYRO] = false;
                timeStepComplete[SENSOR_MAG] = false;
                ScoreFusion.getInstance().setValArray(mValArray);
                ScoreFusion.getInstance().setSensorDataLocked(true);
                // Trigger inference thread
                mInferHandler.post(inferTask);
            }


        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            switch (sensor.getType()) {
                case Sensor.TYPE_ACCELEROMETER:
                    Log.d(TAG, "Accelerometer accuracy changed");
                case Sensor.TYPE_GYROSCOPE:
                    Log.d(TAG, "Gyroscope accuracy changed");
                case Sensor.TYPE_MAGNETIC_FIELD:
                    Log.d(TAG, "Magnetic sensor accuracy changed");
            }

        }
    }


    private TimerTask inferTask = new TimerTask() {
        @Override
        public void run() throws RuntimeException {
            // Freeze mValArray

            // Tensors are row-major; iterate over each time step before each sensor
            // Middle loop = iterate over sensors
            if (ScoreFusion.getInstance().isSensorDataLocked()) {
                for (float[][] sensor : ScoreFusion.getInstance().getValArray()) {
                    for (float[] axis : sensor) {
                        for (float timeStep : axis) {
                            sensorBuffer.put(timeStep);
                        }
                    }
                }
                // Inference
                TensorVector outputs = new TensorVector();
                Status status;
                // TODO: Put the run times in the Results chapter of the paper
                long startTime = System.currentTimeMillis();
                status = sessionSensor.Run(new StringTensorPairVector(new String[]{INPUT_STRING_LSTM},
                        new Tensor[]{sensorTensor}), new StringVector(OUTPUT_STRING_LSTM), new StringVector(), outputs);
                long endTime = System.currentTimeMillis();
                float duration = (endTime - startTime);  //divide by 1000000 to get milliseconds.
                Log.d("LSTM Session", "Session run time: " + duration + " ms");
                if (!status.ok()) {
                    throw new RuntimeException(status.error_message().getString());
                }

                FloatBuffer output = outputs.get(0).createBuffer();
                float[] outputArray = new float[output.limit()];
                output.get(outputArray);
                ScoreFusion.getInstance().setArrayLSTM(outputArray);

                for (int k = 0; k < output.limit(); ++k) {
                    Log.d("LSTM Inference", labelsLSTM[k] + " = " + output.get(k));
                }
                Arrays.sort(outputArray);
                sensorBuffer.clear();
                // Re-register sensor listeners with sampling frequency for gaming
//      mSenseManager.registerListener(mListener, mAccel, SensorManager.SENSOR_DELAY_GAME);
//      mSenseManager.registerListener(mListener, mGyro, SensorManager.SENSOR_DELAY_GAME);
//      mSenseManager.registerListener(mListener, mMag, SensorManager.SENSOR_DELAY_GAME);
                outputs.deallocate();
                output.clear();
                ScoreFusion.getInstance().setSensorDataLocked(false);
            }

        }
    };

    private static boolean allTrue(boolean[] values) {
        for (boolean value : values) {
            if (!value)
                return false;
        }
        return true;
    }
}
