package org.tensorflow.demo;

import android.util.Log;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by reich_canlas on 1/9/2017.
 */
public class ScoreFusion {
    private final String TAG = "SensorFusion";
    private final int SENSOR_NUM = 3; // 0 = accel, 1 = gyro, 2 = magnetic
    private final int AXIS_NUM = 3; //  0 = x, 1 = y, 2 = z
    private final int WINDOW_SIZE = 75;
    private float scoreCNN;
    private float scoreLSTM;
    private float[] arrayLSTM;
    private List<Classifier.Recognition> listRecognition;
    private boolean sensorDataLocked, fusionDisplayed;
    private float[][][] valArray = new float[SENSOR_NUM][AXIS_NUM][WINDOW_SIZE];

    private ScoreFusion() {
        this.scoreCNN = 0;
        this.scoreLSTM = 0;
    }
    private static ScoreFusion instance = null;

    static ScoreFusion getInstance() {
        if (instance == null){
            instance = new ScoreFusion();
        }
        return instance;
    }

    void fusion(){
        // Get max of LSTM scores
        List l = Arrays.asList(this.arrayLSTM);
        this.scoreLSTM = (float)Collections.max(l);


        // Already pre-sorted, just get the first element
        this.scoreCNN = listRecognition.get(0).getConfidence();

        // TODO: Fusion itself!
        Log.d(TAG,"Sensor fusion score: ");
    }
    void setArrayLSTM(float[] arrayLSTM) {
        this.arrayLSTM = arrayLSTM;
    }

    public List<Classifier.Recognition> getListRecognition() {
        return listRecognition;
    }

    void setListRecognition(List<Classifier.Recognition> listRecognition) {
        this.listRecognition = listRecognition;
    }

    float[][][] getValArray() {
        return valArray;
    }

    void setValArray(float[][][] valArray) {
        this.valArray = valArray;
    }

    boolean isSensorDataLocked() {
        return sensorDataLocked;
    }

    void setSensorDataLocked(boolean sensorDataLocked) {
        this.sensorDataLocked = sensorDataLocked;
    }
}
